﻿using PatronMediator;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMediator
{
    // como vemos  en esta clase la hemos herenciado con la interface "IMediator"
    class Mediator : IMediator 
    {
        private List<IColega> colegas; // esta seccion va actuar como comunicador 

        public Mediator()
        {
            colegas = new List<IColega>(); // Metodo constructor 
        }

        // resector  de mensajes  de colegas de las clases astractas 
        public void Add(IColega colega)
        {
            this.colegas.Add(colega);

        }
        // en este ultimo metodo el foreach va  hacer  un recorrido de todos los colegas 
        public void Send(string message, IColega colega)
        {
            foreach (var c in this.colegas) // la variable "c" representa  la palabra  colega 
            {
                if (colega != c)  // luego conparamos lo que se esta recibiendo 
                {
                    c.Recelve(message);  // se recibe el mensaje del metodo astracto de la interface colega 
                }
            }
        }
    }
}
