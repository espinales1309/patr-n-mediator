﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMediator
{

    // como vemos  en esta clase la hemos herenciado con la interface "IColega"
    public class PersonalAdmin: IColega 
    {
        public PersonalAdmin (IMediator mediator) : base(mediator)
        {

        }
        // este metodo es el que va a recibir el mensaje 
        public override void Recelve(string message) 
        {
            // aqui se mostraran  los  mensajes recibido 
            Console.WriteLine("Un personal administrativo recibe····: " + message);
            Console.WriteLine("Se le  notifica por e-mail····· " + message);
        }
    }
}
