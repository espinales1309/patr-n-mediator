﻿using PatronMediator;
using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMediator
{
    //como vemos  en esta clase la hemos herenciado con la interface "IColega"
    class Personal :IColega
    {
        // este metodo a diferecia de los otros tienen diferentes funcionalidades
        public Personal(IMediator mediator) : base(mediator)
        {

        }

        // este metodo funciona como un medio por el cual se comunican los objetos 
        public override void Recelve(string message)
        {
            Console.WriteLine("Un persona  recive" + message);
        }
    }
}
