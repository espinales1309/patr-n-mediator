﻿using System;

namespace PatronMediator
{
    class programs
    {
        static void Main(string[] args)
        {
            Mediator mediador = new Mediator(); // Patron de diseño

            IColega Jonathan = new Personal(mediador); // recibe3 mediador 
            IColega oPersonalAdmin = new PersonalAdmin(mediador);
            IColega oPersonalAdmin2 = new PersonalAdmin(mediador);


            mediador.Add(Jonathan); // recibe un objeto que ha sido inplementado por la interface colega 
            mediador.Add(oPersonalAdmin);
            mediador.Add(oPersonalAdmin2);

            Jonathan.communicate("Administrador, me ayuda con un problema, por favor  "); //mensaje al administrador 
        }
    }
}

