﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatronMediator
{    
    // todos los objetos  que van  a interactuar con el mediador 
    // tiene que inplementar esta interface 

    // para la parte de esta interface Colega utilizaremos una clase de 
    // de modo astracta para agregar nuevas funcionalidades 
    public abstract class IColega
    {
        private IMediator mediator;

        public IMediator Mediator // se crean propiedad de tipo get que no pueda ser  editada 
        {
            get;
        }

        public IColega(IMediator mediator) // el mediador  es rcibido en el constructor 
        {
            this.mediator = mediator;
        }

        // aqui se crea un metodo para comunicar 
        
        // pero este  metodo  no se va a comunicar  con  todos los demas 
        // sino que  comunica lo que  hace atravez del mediador, es decir  el mensaje 
        public void communicate(string message)
        {
            this.mediator.Send(message, this);
        }

        public abstract void Recelve(string message);

    }
}
